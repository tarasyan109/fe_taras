var gulp = require('gulp'),
   sass = require('gulp-sass')(require('sass'));

gulp.task('scss', function () {
   return gulp.src('scss/**/*.scss')
      .pipe(sass())
      .pipe(gulp.dest('css/'));
});

gulp.task('default', function () {
   gulp.watch('scss/styles.scss', gulp.series('scss'));
});